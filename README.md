# Min-max z przycinaniem alfa-beta

## Zadanie

Zaimplementować algorytm min-max z przycinaniem alfa-beta. Algorytm ten należy zastosować do gry w proste warcaby (checekers/draughts). Niech funkcja oceny planszy zwraca różnicę pomiędzy stanem planszy gracza a stanem przeciwnika. Za pion przyznajemy 1 punkt, za damkę 10 p.

Zasady gry (w skrócie: wszyscy ruszają się po 1 polu. Pionki tylko w kierunku wroga, damki w dowolnym) z następującymi modyfikacjami:

- bicie nie jest wymagane
- dozwolone jest tylko pojedyncze bicie (bez serii).

## Pytania

Czy gracz sterowany przez AI zachowuje się rozsądnie z ludzkiego punktu widzenia? Jeśli nie to co jest nie tak?

Niech komputer gra z komputerem (bez wizualizacji), zmieniamy parametry jednego z oponentów, badamy jak zmiany te wpłyną na liczbę jego wygranych. 
Należy zbadać wpływ:
- głębokości drzewa przeszukiwań
- alternatywnych funkcji oceny stanu

## Odpowiedzi

Gracz sterowany przez AI zdaje się grać rozsądnie w krótkiej perspektywie, tzn. zbija wolne piony,
przesuwa piony tak, aby nie dało się ich zbić. W sytuacji, gdy na przykład jest wolna częśc planszy i można wypromować damkę,
ale wymaga to 4 ruchów, AI tego nie widzi. Często gra kończy się, gdy nie ma możliwości ruchu, 
a nie gdy zostają zbite wszystkie piony. Zazwyczaj, choć jedna ze stron sterowanych przez AI ma przewagę,
nie dąży do końca gry, tylko obie strony powtarzają swoje ruchy.

### Wpływ głębokości przeszukiwań

Im większa głębokość tym lepiej, jednak każde zwiększenie głębokości wydłuża wielokrotnie czas podjęcia decyzji.
Zdarza się, że pomimo różnicy głębokości dochodzi do remisu.

### Badanie wpływu głębokości

|B\C | 1 | 2 | 3 | 4 | 5 |
|----|---|---|---|---|---|
|1   | X | C | C | C | C |
|2   | B | X | R | C | C |
|3   | B | B | X | R | C |
|4   | B | R | B | X | C |
|5   | B | B | B | B | X |

C: wygrana czarnych

B: wygrana białych

R: remis

### Alternatywne funkcje oceny stanu   

1. Standardowa: 1 za pion, 10 za damkę
2. Agresywna: 1-8 za pion, w zależności od bliskości promocji na damkę, 50 za damkę 
3. Defensywna: 1 za pion + 1 za sąsiadowanie ze ścianą lub pionem o tym samym kolorze, 
               -1 za sąsiadowanie z pionem o innym kolorze, *3 za damkę
4. Głupia: każdy stan ma taką samą ocenę (wykonuje losowy ruch)

Zastosowana funkcja oceny stanu ma większe znaczenie od głębokości - AI z lepszą funkcją stanu wygrywa z AI ze słabszą 
pomimo mniejszej głębokości. 


### Badanie wpływu użytej heurystyki

|B\C | 1 | 2 | 3 | 4 |
|----|---|---|---|---|
|1   | X | R | C | B |
|2   | R | X | R | B |
|3   | C | C | X | R |
|4   | C | C | B | X |


Funkcje heurystyczne standardowa i agresywna są lepsze niż defensywna i głupia, jednak nie można stwierdzić, 
że są lepsze dla każdej oceny planszy.
