import copy
from random import random, choice


class Vector2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, v2):
        return Vector2(self.x + v2.x, self.y + v2.y)

    def __sub__(self, v2):
        return Vector2(self.x - v2.x, self.y - v2.y)

    def __mul__(self, num):
        return Vector2(self.x * num, self.y * num)

    def __eq__(self, v2):
        return self.x == v2.x and self.y == v2.y


class Piece:
    def __init__(self, x, y, is_white):
        self.position = Vector2(x, y)
        self.is_white = is_white
        self.is_king = False

    def __str__(self):
        return ('⛁ ' if self.is_king else '⛀ ') if self.is_white else ('⛃ ' if self.is_king else '⛂ ')

    def getMoveDirections(self):
        if self.is_king:
            return [Vector2(1, 1), Vector2(1, -1), Vector2(-1, 1), Vector2(-1, -1)]
        if self.is_white:
            return [Vector2(1, 1), Vector2(-1, 1)]
        return [Vector2(1, -1), Vector2(-1, -1)]

    def getPosition(self):
        return self.position

    def setPosition(self, newPosition):
        self.position = newPosition
        if newPosition.y in [0, 7]:
            self.is_king = True


class Board:
    def __init__(self,
                 white_pieces=[Piece(i % 4 * 2 + i // 4 % 2, i // 4, True)
                               for i in range(12)],
                 black_pieces=[Piece(i % 4 * 2 + (i // 4 + 1) %
                                     2, 7 - i // 4, False) for i in range(12)],
                 white_to_move=True):

        self.white_pieces = white_pieces
        self.black_pieces = black_pieces
        self.white_to_move = white_to_move
        self.board_size = 8

    def getAllPieces(self):
        return self.white_pieces + self.black_pieces

    def getBoardRepresentation(self):
        board = [['⬜' if (i + j) % 2 else '⬛' for i in range(8)]
                 for j in range(8)]
        for piece in self.white_pieces:
            board[piece.getPosition().y][piece.getPosition().x] = str(piece)
        for piece in self.black_pieces:
            board[piece.getPosition().y][piece.getPosition().x] = str(piece)
        return board

    def printBoard(self):
        print()
        for row in self.getBoardRepresentation():
            print(*row)

    def generateAllPossibleNextBoards(self):
        new_boards = []
        for piece in (self.white_pieces if self.white_to_move else self.black_pieces):
            move_directions = piece.getMoveDirections()
            for direction in move_directions:
                new_position = piece.getPosition() + direction

                if not self.isInBoardRange(new_position):
                    continue
                if (neighbor := self.getPieceByPosition(new_position)) is None:
                    new_board = copy.deepcopy(self)
                    new_boards.append(new_board)
                    new_board.movePiece(piece.getPosition(), new_position)
                    new_board.white_to_move = not new_board.white_to_move
                else:
                    if neighbor.is_white != self.white_to_move:
                        new_position += direction
                        if self.isInBoardRange(new_position) and self.isSquareEmpty(new_position):
                            new_board = copy.deepcopy(self)
                            new_boards.append(new_board)
                            new_board.movePiece(
                                piece.getPosition(), new_position)
                            new_board.removePiece(
                                new_position - direction)
                            new_board.white_to_move = not new_board.white_to_move

        return new_boards

    def getPieceByPosition(self, position):
        for piece in self.getAllPieces():
            if piece.getPosition() == position:
                return piece

    def isSquareEmpty(self, position):
        return self.getPieceByPosition(position) is None

    def isPieceOnSquare(self, position):
        return not self.isSquareEmpty(position)

    def isInBoardRange(self, position):
        return position.x >= 0 and position.y >= 0 and position.x < self.board_size and position.y < self.board_size

    def movePiece(self, old_position, new_position):
        if (piece := self.getPieceByPosition(old_position)) is not None:
            piece.setPosition(new_position)

    def removePiece(self, position):
        self.black_pieces = list(filter(
            lambda x: x.getPosition() != position, self.black_pieces))
        self.white_pieces = list(filter(
            lambda x: x.getPosition() != position, self.white_pieces))

    def __lt__(self, other):
        return random() < 0.5


class AI:
    def __init__(self, depth: int):
        self.depth = depth

    def makeMove(self, board: Board):
        successors = board.generateAllPossibleNextBoards()
        α, β = -100000, 100000

        if board.white_to_move:
            return max(map(lambda successor: (self.alfaBeta(successor, self.depth - 1, α, β), successor), successors))[1]
        else:
            return min(map(lambda successor: (self.alfaBeta(successor, self.depth - 1, α, β), successor), successors))[1]

    def _board_value(self, board: Board):
        return sum(map(lambda piece: self._piece_value(piece,  board), board.getAllPieces()))

    def _piece_value(self, piece: Piece, board: Board = None):
        return (1 if piece.is_white else -1) * (10 if piece.is_king else 1)

    def alfaBeta(self, board: Board, depth: int, α: int, β: int):
        successors = board.generateAllPossibleNextBoards()

        if len(successors) == 0:
            return -100000 if board.white_to_move else 100000

        if depth == 0:
            return self._board_value(board)

        if board.white_to_move:
            for successor in successors:
                α = max(α, self.alfaBeta(successor, depth - 1, α, β))
                if α >= β:
                    return β
            return α
        else:
            for successor in successors:
                β = min(β, self.alfaBeta(successor, depth - 1, α, β))
                if α >= β:
                    return α
            return β


class StupidAI:
    def makeMove(self, board: Board):
        return choice(board.generateAllPossibleNextBoards())


class AggressiveAI(AI):
    def __init__(self, depth: int):
        self.depth = depth

    def _piece_value(self, piece: Piece, board: Board = None):
        if piece.is_white:
            return 50 if piece.is_king else piece.getPosition().y
        return -50 if piece.is_king else piece.getPosition().y - 7


class DefensiveAI(AI):
    def __init__(self, depth: int):
        self.depth = depth

    def _piece_value(self, piece: Piece, board: Board = None):
        value = 1

        for direction in [Vector2(1, 1), Vector2(-1, 1), Vector2(1, -1), Vector2(-1, -1)]:
            position = piece.getPosition() + direction
            if board.isInBoardRange(position):
                value += 1
            elif board.isPieceOnSquare(position):
                if board.getPieceByPosition(position).is_white == board.white_to_move:
                    value += 1
                else:
                    value -= 1

        if not board.white_to_move:
            value *= -1

        if piece.is_king:
            value *= 3

        return value


class Game:
    def __init__(self, players):
        self.board = Board()
        self.players = players
        self.move_counter = 0

    def start(self):
        while not self.isGameFinished():
            self.board.printBoard()
            self.board = self.players[0 if self.board.white_to_move else 1].makeMove(
                self.board)
            self.move_counter += 1

            if self.move_counter == 200:
                print(0)
                self.board.printBoard()
                return

        print(f"{'BLACK' if self.board.white_to_move else 'WHITE'} WINS")
        self.board.printBoard()

    def isGameFinished(self):
        return len(self.board.generateAllPossibleNextBoards()) == 0


if __name__ == "__main__":
    game = Game([AI(1), AggressiveAI(3)])
    game.start()
